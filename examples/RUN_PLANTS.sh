# ANGES 1.01, reconstructing ANcestral GEnomeS maps
# July 2012.
# Contact: Cedric Chauve (Dept. Mathematics, Simon Fraser University), cedric.chauve@sfu.ca

# Running plant examples

cd plant_genomes/
python3 ../../src/MASTER/anges_CAR.py PARAMETERS_MONOCOTS_BAB
cd ../
